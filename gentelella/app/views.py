from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse, JsonResponse
from django.conf import settings
from django.core.files.storage import FileSystemStorage

from app import SVMPredictingAgent


def index(request):
    context = {}
    template = loader.get_template('app/index.html')
    return HttpResponse(template.render(context, request))


def gentella_html(request):
    context = {}
    # The template to be loaded as per gentelella.
    # All resource paths for gentelella end in .html.

    # Pick out the html file name from the url. And load that template.
    load_template = request.path.split('/')[-1]
    template = loader.get_template('app/' + load_template)
    return HttpResponse(template.render(context, request))


def simple_upload(request):
    if request.method == 'POST' and request.FILES['myfile']:
        myfile = request.FILES['myfile']
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        uploaded_file_url = fs.url(filename)
        return render(request, 'core/simple_upload.html', {
            'uploaded_file_url': uploaded_file_url
        })
    return render(request, 'core/simple_upload.html')


def hello(request):
    return HttpResponse("Hello world")


def analyze_from_button(request):
    # if request.GET.get('analyse_btn'):
    prediction_agent = \
        SVMPredictingAgent.SVMPredictingAgent("poly",
                                              "D:/University/WireHack Mars 2019/wirehack-mars-2019/wirehack-mars-2019/"
                                              "gentelella/app/training_data.xlsx",
                                              "D:/University/WireHack Mars 2019/wirehack-mars-2019/wirehack-mars-2019/"
                                              "gentelella/app/cancer.xlsx")
    cancer_or_no_cancer = prediction_agent.predict_site(3620)
    print(cancer_or_no_cancer)
    return JsonResponse({"value": cancer_or_no_cancer}, safe=False)
