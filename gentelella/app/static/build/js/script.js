const personne = function(firstName,lastName, age,microcalcifications){
    let cancer;
    function setCancer(a){cancer = a;}
    const self={
        getFirstName () {return firstName;},
        getLastName(){return lastName;},
        getAge(){return age;},
        getMicrocalcifications(){return microcalcifications;},
        getCancer(){return cancer;},
        setCancer
    }
    return self;
};

const pers1=personne("Valery", "Stiers", 21, 7);
const pers2=personne("Carla", "Junk", 43, 3);
const pers3=personne("Tamera", "Patchett", 64, 4);
const pers4=personne("Pinkie", "Woldt", 60, 5);
const pers5=personne("Delena", "Hemmings", 65, 4);
const pers6=personne("Candance", "Haapla", 39, 12);
const pers7=personne("Ivonne", "Curatolo", 41, 2);
const pers8=personne("Suzanne", "Angel", 83, 5);
const pers9=personne("Nancee", "Tuten", 53, 1);
const pers10=personne("Zola", "Otey", 30, 13);
const pers11=personne("Jolene", "Errico", 54, 5);
const pers12=personne("Viviana", "Dantin", 68, 8);

let tabPers = [pers1, pers2, pers3, pers4, pers5, pers6, pers7, pers8,
pers9, pers10, pers11, pers12];

for(let i=0;i<12;i++){
    $('tbody').append("<tr class="+"even pointer"+">");
    $('tbody tr:last').append('<td class=" ">'+tabPers[i].getFirstName());
    $('tbody tr:last').append('<td class=" ">'+tabPers[i].getLastName());
    $('tbody tr:last').append('<td class=" ">'+tabPers[i].getAge());
    $('tbody tr:last').append('<td class=" ">'+tabPers[i].getMicrocalcifications());
    
    if(tabPers[i].getFirstName() === "Carla"){
        $('tbody tr:last').append('<td class=" last"><a id="replace" href="#"> Upload');
        $('tbody tr:last').append('<td class=" last"><a href="patient.html"> View');
    }else{
        $('tbody tr:last').append('<td class=" last"><a href="#"> Upload');
        $('tbody tr:last').append('<td class=" "><a href="#"> View');
    } 
}

$('td').on('click', 'a', function(){
    let current = $(this).parent();
    console.log($(this).text());
    $(current).empty();
    $(current).append('<div class="loader">');
    console.log($(current).html());
    kamenLourd(current);
});

const sleep = (milliseconds) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }

  const doSomething = async (pourcentage, current) => {
    console.log(pourcentage);
    $(current).empty();
    $(current).append('<p>');
    for(let i=0;i<pourcentage;i++){
        await sleep(27);
        if(i == 0){
            current.text('0 %');
        }else current.text(i + ' %');
        console.log(i);
    }
    console.log("end of animation");
  }
  
function kamenLourd(current){
    $.ajax({
        url:'/click',
		//async: false,
		dataType: 'json',
        success: function(resp){
			console.log(resp["value"]);
			doSomething(resp["value"]*100, current);
        }
    });
}