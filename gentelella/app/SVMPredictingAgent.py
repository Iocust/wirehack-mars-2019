#!/usr/bin/env python3
from collections import Counter
from sklearn import svm
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import MinMaxScaler
import numpy
import pandas
import logging
import sys
import matplotlib.pyplot as plt
import numpy as np


class SVMPredictingAgent:

    def __init__(self, kernel, training_file, test_file):
        logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
        self.training_file = training_file
        self.test_file = test_file
        self.kernel = kernel
        self.training_samples, self.training_classes,\
            self.training_labels, self.patients_list_training = self.parse_data(training_file)
        self.prediction_samples, self.prediction_classes,\
            self.prediction_labels, self.patients_list_prediction = self.parse_data(test_file)
        self.micro_svm = svm.SVC()

    def predict(self, c_param):
        self.__predict(c_param, self.prediction_samples, self.prediction_classes)

    def predict_site(self, c_param):
        return self.__predict(c_param, self.prediction_samples, [1 for i in range(len(self.prediction_samples))])

    def split_by_patient(self, c_param):
        patient_array = [[] for i in range(int(self.patients_list_training[-1][0]))]
        print("Number of patients: ", patient_array.__len__())
        for i in range(len(self.patients_list_training)):
            patient_array[int(self.patients_list_training[i][0])-1].append(self.patients_list_training[i][1:])
        patient_array = numpy.array(patient_array)

        counter = 0
        sum = 0
        for patient_data in patient_array:
            counter += 1
            sum += self.__predict(c_param, patient_data, [1 if i > 46 else 0 for i in range(len(patient_data))])
            print(sum/counter)

    def __predict(self, c_param, prediction_samples, prediction_classes):
        """
        Predicts the class of the given data using an SVM.
        :param c_param: The error compensation
        :return: The accuracy of the predictions.
        """
        logging.debug("Starting predictions of data.")
        if self.kernel == "poly":
            self.micro_svm = self.init_poly_svm(c_param)
        elif self.kernel == "linear":
            self.micro_svm = self.init_linear_svm(c_param)

        predictions = self.micro_svm.predict(prediction_samples)
        print(predictions)
        accuracy = self.compute_accuracy(prediction_classes, predictions)

        logging.debug("End of predictions.")
        return accuracy

    def init_linear_svm(self, c_param):
        """
            Initializes and trains an SVM to predict whether described microcalcifications in input data are benign or
            malicious.
            :param c_param: The error compensation parameter
            :return: The trained SVM to be used for predictions.
            """
        logging.debug("Initializing linear SVM from: " + self.training_file)

        micro_svm = svm.SVC(kernel="linear", gamma="auto", C=c_param)
        micro_svm.fit(self.training_samples, self.training_classes)

        logging.debug("End of SVM initialization.")
        return micro_svm

    def init_poly_svm(self, c_param, dgr=2):
        """
        Initializes and trains an SVM to predict whether described microcalcifications in input data are benign or
        malicious.
        :return: The trained SVM to be used for predictions.
        """
        logging.debug("Initializing SVM from: " + self.training_file)

        micro_svm = svm.SVC(kernel="poly", degree=dgr, gamma="auto", C=c_param)
        micro_svm.fit(self.training_samples, self.training_classes)

        logging.debug("End of SVM initialization.")
        return micro_svm

    def parse_data(self, filename):
        """
        Parses data from an .xlsx file into a matrix that can be fed to the SVM for training or predictions.
        The data has to be in the following format:
        Patient_ID|... all the data ...|Class
        :param filename: The name of the .xlsx file.
        :return: A triple (data matrix, class matrix, data labels). The data labels are the column names.
        """
        logging.debug("Starting .xlsx parsing.")

        data = pandas.read_excel(filename)
        patient_numbers = data.values[:, :-1]
        data_classes = data.values[:, -1]  # Take last column for class labels (0 = benign, 1 = malignant)
        data_samples = data.values[:, 1:-1]  # Remove first column (patient number) and last column (result)
        data_labels = list(data.columns)  # Labels of all columns
        data_samples = self.preprocess_data(data_samples)

        logging.debug("End of parsing.")
        return data_samples, data_classes, data_labels, patient_numbers

    def k_fold_stratified_test(self, c_param, k):

        #self.split_by_patient(c_param)

        logging.debug("Starting stratified K-fold test with data from: " + self.training_file)

        skf = StratifiedKFold(n_splits=k, shuffle=True)
        X_train = []
        y_train = []
        X_test = []
        y_test = []
        for a, b in (skf.split(self.training_samples, self.training_classes)):
            X_train.append(self.training_samples[a])
            y_train.append(self.training_classes[a])
            X_test.append(self.training_samples[b])
            y_test.append(self.training_classes[b])

        X_train = numpy.array(X_train)
        y_train = numpy.array(y_train)
        X_test = numpy.array(X_test)
        y_test = numpy.array(y_test)
        h = .02
        list_c = []
        list_accuracy = []

        for i in range(k):
            if self.kernel == "poly":
                self.micro_svm = svm.SVC(kernel="poly", degree=2, gamma="auto", C=c_param)
                self.micro_svm.fit(X_train[i], y_train[i])
            elif self.kernel == "linear":
                self.micro_svm = svm.SVC(kernel="linear", gamma="auto", C=c_param)
                self.micro_svm.fit(X_train[i], y_train[i])
            list_c.append(i)
            list_accuracy.append(self.__predict(c_param, X_test[i], y_test[i]))

        k_fold_mean = [numpy.mean(list_accuracy)]*len(list_c)
        return k_fold_mean[0]


    @staticmethod
    def preprocess_data(data_matrix):
        """
        Normalizes the data before the SVM can process it. This is a necessary step, without which the SVM never reaches
        the end of its training or prediction.
        :param data_matrix: The matrix with the data to be normalized
        :return: The matrix with normalized data
        """
        logging.debug("Starting data normalization.")

        scaler = MinMaxScaler()  # Used to normalize data between 0 and 1
        new_matrix = scaler.fit_transform(data_matrix)

        logging.debug("End of data normalization.")
        return new_matrix

    @staticmethod
    def compute_accuracy(facts, predictions):
        """
        Computes the accuracy between a given list of facts (class labels from data matrix) and the SVM predictions about
        the same data. This is used on data that is unknown to the SVM, i.e. not used in the training set.
        :param facts: The class labels that are known to be true.
        :param predictions: The predictions obtained from the SVM.
        :return: The accuracy ratio of the predictions.
        """
        logging.debug("Computing accuracy.")
        prediction_counter = Counter(predictions)
        return (sum(min(n, prediction_counter[l]) for l, n in Counter(facts).items())) / (facts.__len__())
